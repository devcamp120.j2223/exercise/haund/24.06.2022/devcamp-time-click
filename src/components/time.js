import { Component } from "react";

let today = new Date();
let hour = today.getHours();
let minute = today.getMinutes();
let second = today.getSeconds();
let statusDay = hour >= 12 ? "PM" : "AM";

class Time extends Component {
    constructor(props) {
        super(props);
        this.state = {time: []}
    }

    onBtnChangeColor = () =>{
        let today = new Date();
        let hour = today.getHours();
        let minute = today.getMinutes();
        let second = today.getSeconds();
        let statusDay = hour >= 12 ? "PM" : "AM";

        let timeAdd = "";

        timeAdd = `${hour}:${minute}:${second} - ${statusDay}`
        this.setState({
           time: [...this.state.time, timeAdd]
       })
    }

    render(){
        // let today = new Date();
        return(
            <div>
                <h2>Hello World</h2>
                <h4 style={{color: this.state.color }}>Time is {hour}:{minute}:{second} - {statusDay}</h4>
                <div className="row mt-3">
                    {this.state.time.map((element, index) => {
                        return <p key={index}>{element}</p>
                    })}
                </div>
                <button onClick={this.onBtnChangeColor}>Add to list</button>
            </div>
        )
    }
}

export default Time;